package com.munaw.bme.chat.server.monitoring;

import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.server.client.LogClient;
import com.munaw.bme.chat.server.client.MesssagePersisterClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class MonitoringManagerTest {
	@Mock
	private SubscriptionService subscriptionService;
	@Mock
	private LogClient logClient;
	@Mock
	private MesssagePersisterClient messagePersisterClient;

	@Test
	public void testInit() throws Exception {
		MonitoringManager monitoringManager = new MonitoringManager(subscriptionService, logClient, messagePersisterClient);
		monitoringManager.init();
		Mockito.verify(subscriptionService, times(1)).register(logClient);
		Mockito.verify(subscriptionService, times(1)).register(messagePersisterClient);
	}
}