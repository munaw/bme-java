package com.munaw.bme.chat.server.client;

import com.munaw.bme.chat.api.Channel;
import com.munaw.bme.chat.api.Distributor;
import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.api.message.MessageImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ClientTest {


	@Mock
	private Distributor distributor;
	@Mock
	private Channel channel;
	@Mock
	private SubscriptionService subscriptionService;


	@Test
	public void testOnMessageForwarded() throws Exception {
		Client client = getSut();
		Message testMessage = getTestMessage();
		client.onMessage(testMessage, subscriptionService);
		verify(channel, times(1)).send(testMessage);
	}

	@Test
	public void testOnMessageWhenConnectionLost() throws Exception {
		Client client = getSut();
		Message testMessage = getTestMessage();
		Mockito.doThrow(IOException.class).when(channel).send(any(Message.class));
		client.onMessage(testMessage, subscriptionService);
		verify(subscriptionService, times(1)).unregister(client);
	}

	@Test
	public void testRunReceiveMessage() throws Exception {
		Client client = getSut();
		final Message testMessage = getTestMessage();
		Thread thread = new Thread(client);
		try {
			when(channel.receive()).then(new Answer<Message>() {
				int callCount = 0;

				@Override
				public Message answer(final InvocationOnMock invocationOnMock) throws Throwable {
					if (callCount++ > 0) {
						throw new InterruptedException(); // masodik hivasra interrupt
					}
					Thread.sleep(10); // blocking metodus
					return testMessage;
				}
			});
			thread.start();
			thread.join(); // bevarom
			verify(distributor, times(1)).put(client, testMessage);
		} finally {
			thread.interrupt();
		}
	}

	private Message getTestMessage() {
		return new MessageImpl.Builder().withSender("test").withText("test text").build();
	}

	Client getSut() {
		return new Client(distributor, channel);
	}
}