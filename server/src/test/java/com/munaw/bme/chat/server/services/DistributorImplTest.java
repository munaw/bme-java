package com.munaw.bme.chat.server.services;

import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.api.consumer.Consumer;
import com.munaw.bme.chat.api.message.DefaultPartyId;
import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.api.message.MessageImpl;
import com.munaw.bme.chat.server.client.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DistributorImplTest {

	@Mock
	private SubscriptionService subscriptionService;

	@Mock
	Client client1;
	@Mock
	Client client2;
	@Mock
	Client client3;
	@Mock
	Client client4;


	@Before
	public void setUp() throws Exception {
		long id = 0;
		for (Client client : getClients()) {
			id++;
			when(client.getConsumerId()).thenReturn(new DefaultPartyId(id));
			when(client.getProducerId()).thenReturn(new DefaultPartyId(id));
		}


	}

	@Test
	public void testPut() throws Exception {
		DistributorImpl sut = getSut();
		when(subscriptionService.getConsumers())
				.thenReturn(getConsumers());
		Message testMessage1 = getTestMessage(1L);
		Message testMessage2 = getTestMessage(2L);

		sut.put(client1, testMessage1);
		sut.put(client2, testMessage2);

		Thread thread = new Thread(sut);
		thread.start();
		while (sut.isQueued()) {
			Thread.sleep(1); // kju urulesre varva
		}
		thread.interrupt();

		verify(client1, never()).onMessage(testMessage1, subscriptionService);
		verify(client2, times(1)).onMessage(testMessage1, subscriptionService);
		verify(client3, times(1)).onMessage(testMessage1, subscriptionService);
		verify(client4, times(1)).onMessage(testMessage1, subscriptionService);

		verify(client1, times(1)).onMessage(testMessage2, subscriptionService);
		verify(client2, never()).onMessage(testMessage2, subscriptionService);
		verify(client3, times(1)).onMessage(testMessage2, subscriptionService);
		verify(client4, times(1)).onMessage(testMessage2, subscriptionService);
	}


	private List<Consumer> getConsumers() {
		return Arrays.asList(client1, client2, client3, client4);
	}

	private List<Client> getClients() {
		return Arrays.asList(client1, client2, client3, client4);
	}

	private DistributorImpl getSut() {
		return new DistributorImpl(100, subscriptionService);
	}


	private Message getTestMessage(Long id) {
		return new MessageImpl.Builder().withId(id).withSender("test").withText("test text").build();
	}
}