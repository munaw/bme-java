package com.munaw.bme.chat.server.services;

import com.munaw.bme.chat.api.consumer.Consumer;
import com.munaw.bme.chat.api.message.DefaultPartyId;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SubscriptionServiceImplTest {
	@Mock
	private Consumer consumer1;
	@Mock
	private Consumer consumer2;
	@Mock
	private Consumer consumer3;

	@Before
	public void setUp() throws Exception {
		when(consumer1.getConsumerId()).thenReturn(new DefaultPartyId(1L));
		when(consumer2.getConsumerId()).thenReturn(new DefaultPartyId(2L));
		when(consumer3.getConsumerId()).thenReturn(new DefaultPartyId(3L));
	}

	@Test
	public void testRegister() throws Exception {
		SubscriptionServiceImpl sut = new SubscriptionServiceImpl();
		sut.register(consumer1);
		sut.register(consumer2);
		assertTrue(sut.getConsumers().equals(Arrays.asList(new Consumer[]{consumer1, consumer2})));
	}

	@Test
	public void testUnregister() throws Exception {
		SubscriptionServiceImpl sut = new SubscriptionServiceImpl();
		sut.register(consumer1);
		sut.register(consumer2);
		sut.register(consumer3);
		assertTrue(sut.getConsumers().equals(Arrays.asList(new Consumer[]{consumer1, consumer2, consumer3})));
		sut.unregister(consumer1);
		assertTrue(sut.getConsumers().equals(Arrays.asList(new Consumer[]{consumer2, consumer3})));
		sut.unregister(consumer3);
		assertTrue(sut.getConsumers().equals(Arrays.asList(new Consumer[]{consumer2})));

	}

	/**
	 * verify works with copy-on-get
	 */
	@Test
	public void testGetConsumers() throws Exception {
		SubscriptionServiceImpl sut = new SubscriptionServiceImpl();
		sut.register(consumer1);
		sut.register(consumer2);
		sut.register(consumer3);
		List<Consumer> consumers = sut.getConsumers();
		sut.unregister(consumer1);
		assertTrue(consumers.equals(Arrays.asList(new Consumer[]{consumer1, consumer2, consumer3})));
	}
}