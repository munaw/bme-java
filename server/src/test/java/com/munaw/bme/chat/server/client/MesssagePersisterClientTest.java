package com.munaw.bme.chat.server.client;

import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.server.store.MessageDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MesssagePersisterClientTest {

	@Mock
	private MessageDao messageDao;
	@Mock
	private Message message;
	@Mock
	private SubscriptionService subscriptionService;

	@Test
	public void testOnMessage() throws Exception {
		MesssagePersisterClient messsagePersisterClient = new MesssagePersisterClient(messageDao);
		messsagePersisterClient.onMessage(message, subscriptionService);
		verify(messageDao, times(1)).store(message);
	}
}