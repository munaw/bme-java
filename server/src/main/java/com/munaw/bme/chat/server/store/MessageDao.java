package com.munaw.bme.chat.server.store;

import com.munaw.bme.chat.api.message.Message;

/**
 * MessageDao interfész. Egyedül a tárolás metódusa került deklarálásra.
 * @author munaw
 */
public interface MessageDao {
	/**
	 * Letárolja az atáadott üzenetet.
	 * @param message
	 */
	void store(Message message);
}
