package com.munaw.bme.chat.server;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * Konfigurációt felolvasó és azt tároló osztály.
 * Konstruktor paraméterként fogad alternatív argumentumokat, melyek felülírják a felolvasott beállításokat.
 * @author munaw
 */
public class Config {
	final private Properties properties;

	final private int port;
	final private int maxClient;

	final private String dbDrive;
	final private String dbConnection;


	public Config(final String filename, final Map<String, String> argsMap) throws IOException {
		properties = new Properties();
		properties.load(getClass().getClassLoader().getResourceAsStream(filename));
		port = argsMap.containsKey("port")
				? Integer.parseInt(argsMap.get("port"))
				: Integer.parseInt(properties.getProperty("port"));
		maxClient = argsMap.containsKey("max-client")
				? Integer.parseInt(argsMap.get("max-client"))
				: Integer.parseInt(properties.getProperty("max_client"));
		dbDrive = argsMap.containsKey("db-driver")
				? argsMap.get("db-driver")
				: properties.getProperty("db.driver");
		dbConnection = argsMap.containsKey("db-connection")
				? argsMap.get("db-connection")
				: properties.getProperty("db.connection");


	}

	public int getMaxClient() {
		return maxClient;
	}

	public int getPort() {
		return port;
	}

	public String getDbDrive() {
		return dbDrive;
	}

	public String getDbConnection() {
		return dbConnection;
	}
}
