package com.munaw.bme.chat.server.monitoring;

import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.server.client.LogClient;
import com.munaw.bme.chat.server.client.MesssagePersisterClient;

/**
 * Két Consumert kapcsol a SubscriptionService-re, ezzel azok számára is láthatóvá téve a chat-folyamot.
 * A két consumer loggolást és adatbázis mentést végzi.
 *
 * @author munaw
 */
public class MonitoringManager {
	final private SubscriptionService subscriptionService;
	final private LogClient logClient;
	final private MesssagePersisterClient messsagePersisterClient;

	public MonitoringManager(final SubscriptionService subscriptionService, final LogClient logClient, final MesssagePersisterClient messsagePersisterClient) {
		this.subscriptionService = subscriptionService;
		this.logClient = logClient;
		this.messsagePersisterClient = messsagePersisterClient;
	}

	public void init() {
		subscriptionService.register(logClient);
		subscriptionService.register(messsagePersisterClient);
	}
}
