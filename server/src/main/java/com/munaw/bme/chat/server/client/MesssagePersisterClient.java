package com.munaw.bme.chat.server.client;

import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.api.consumer.Consumer;
import com.munaw.bme.chat.api.consumer.ConsumerId;
import com.munaw.bme.chat.api.message.DefaultPartyId;
import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.server.store.MessageDao;
import org.apache.log4j.Logger;

/**
 * Fogyasztó implementáció az üzenetek perzisztálására. Az üzeneteket a MessageDao felé továbbítja.
 *
 * @author munaw
 */
public class MesssagePersisterClient implements Consumer {
	private final ConsumerId consumerId = new DefaultPartyId(Client.IdGenerator.generateId());

	private final MessageDao messageDao;

	private final static Logger logger = Logger.getLogger(LogClient.class);

	public MesssagePersisterClient(final MessageDao messageDao) {
		this.messageDao = messageDao;
	}

	@Override
	public ConsumerId getConsumerId() {
		return consumerId;
	}

	@Override
	public void onMessage(final Message message, final SubscriptionService subscriptionService) {
		messageDao.store(message);
	}

}
