package com.munaw.bme.chat.server.services;

import com.munaw.bme.chat.api.Distributor;
import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.api.consumer.Consumer;
import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.api.producer.Producer;
import com.munaw.bme.chat.api.producer.ProducerId;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Az osztály a továbbítandó üzeneteket fogadja, majd azokat egy szinkronizált
 * queue-ban gyűjti. Emellett az osztályt threadben futtatva a példány
 * a fogadott message-eket elküldi a SubscriptionService által visszaadott Consumereknek
 *
 * @author munaw
 */
public class DistributorImpl implements Distributor, Runnable {

	final private BlockingQueue<MessagePack> messages;
	final private SubscriptionService subscriptionService;


	public DistributorImpl(int queueLimit, SubscriptionService subscriptionService) {
		this.messages = new LinkedBlockingQueue(queueLimit);
		this.subscriptionService = subscriptionService;
	}

	@Override
	public void put(final Producer sender, final Message message) {
		messages.add(new MessagePack(sender.getProducerId(), message));
	}

	@Override
	public void run() {
		MessagePack messagePack;
		while (true) {
			try {
				messagePack = messages.take();
				sendMessage(messagePack);

			} catch (InterruptedException e) {
				return;
			}
		}
	}

	private void sendMessage(final MessagePack messagePack) {
		List<Consumer> copyOfConsumers = subscriptionService.getConsumers();

		for (Consumer consumer : copyOfConsumers) {
			if (!consumer.getConsumerId().equals(messagePack.getProducerId())) {
				consumer.onMessage(messagePack.getMessage(), subscriptionService);
			}
		}
	}


	public boolean isQueued() {
		return messages.size() > 0;
	}

	static private class MessagePack {
		final private ProducerId producerId;
		final private Message message;

		private MessagePack(final ProducerId producerId, final Message message) {
			this.producerId = producerId;
			this.message = message;
		}

		public Message getMessage() {
			return message;
		}

		public ProducerId getProducerId() {
			return producerId;
		}
	}
}
