package com.munaw.bme.chat.server.services;

import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.api.consumer.Consumer;

import javax.annotation.concurrent.ThreadSafe;
import java.util.ArrayList;
import java.util.List;

/**
 * ThreadSafe SubscriptionService implementáció.
 * A subsriber lista szinkronizált eléréssel kerül módosításra, emellett olvasáskor
 * másolat készül a listáról, így a felolvasás közbeni módosítások nem okoznak ütközést.
 *
 * @author munaw
 */
@ThreadSafe
public class SubscriptionServiceImpl implements SubscriptionService {

	final private List<Consumer> consumers;

	public SubscriptionServiceImpl() {
		this.consumers = new ArrayList<>();
	}

	@Override
	public synchronized void register(final Consumer consumer) {
		consumers.add(consumer);
	}

	@Override
	public synchronized void unregister(final Consumer consumer) {
		consumers.remove(consumer);
	}

	@Override
	public synchronized List<Consumer> getConsumers() {
		return new ArrayList<>(consumers);
	}
}
