package com.munaw.bme.chat.server;

import com.munaw.bme.chat.server.dic.ServiceLocator;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Az osztály indítja az alkalmazást. Felparszolja a paramétereket, betölti az alapértelmezett konfigokat
 * és eldinítja a szervert.
 * Emellett itt kerül inicializálásra a MonitoringManager is, mely a logolást és az archiválást kezeli.
 *
 * @author munaw
 */
public class ServerApplication {
	final static private Logger logger = Logger.getLogger(ServerApplication.class);

	public static void main(String[] args) {
		try {
			Cli cli = new Cli();
			Map<String, String> argsMap = null;
			try {
				argsMap = cli.parse(args);
			} catch (ParseException e) {
				e.printStackTrace();
				return;
			}
			if (argsMap.containsKey("help")) {
				cli.help();
				return;
			}
			Config config = new Config("application.properties", argsMap);
			ServiceLocator serviceLocator = new ServiceLocator(config);
			serviceLocator.getMonitoringManager().init();
			try {
				serviceLocator.getServer().start();
			} catch (IOException | JAXBException e) {
				logger.error(e);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static private class Cli {
		private final Options options;

		private Cli() {
			options = new Options();
			options.addOption("h", "help", false, "Help");
			options.addOption("p", "port", true, "A port, amin a szerver figyel");
			options.addOption("d", "db-driver", true, "DB driver");
			options.addOption("c", "db-connection", true, "DB connection string");
			options.addOption("m", "max-client", true, "Max client connection");
		}

		public Map<String, String> parse(String[] args) throws ParseException {
			BasicParser parser = new BasicParser();
			CommandLine cmd = parser.parse(options, args);
			Map<String, String> params = new HashMap<>();
			if (cmd.hasOption("p")) {
				params.put("port", cmd.getOptionValue("p"));
			}
			if (cmd.hasOption("d")) {
				params.put("db-drive", cmd.getOptionValue("d"));
			}
			if (cmd.hasOption("c")) {
				params.put("db-connection", cmd.getOptionValue("c"));
			}
			if (cmd.hasOption("m")) {
				params.put("max-client", cmd.getOptionValue("m"));
			}
			if (cmd.hasOption("h")) {
				params.put("help", cmd.getOptionValue("h"));
			}
			return params;
		}

		private void help() {
			HelpFormatter formater = new HelpFormatter();
			formater.printHelp("server", options);
			System.exit(0);
		}

	}

}
