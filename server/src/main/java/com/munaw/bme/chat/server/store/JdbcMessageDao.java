package com.munaw.bme.chat.server.store;

import com.munaw.bme.chat.api.message.Message;
import org.apache.log4j.Logger;

import java.sql.*;

/**
 * Jdbc alapú MessageDao implementáció. Inicializáláskor létrehozza a tároló-tábláját.
 * Sqlite szintaxist követ.
 * @author munaw
 */
public class JdbcMessageDao implements MessageDao {
	final static private Logger logger = Logger.getLogger(JdbcMessageDao.class);
	final private Connection connection;

	public JdbcMessageDao(String dbDrive, String dbConnection) throws ClassNotFoundException, SQLException {
		Class.forName(dbDrive);
		this.connection = DriverManager.getConnection(dbConnection);
		Statement statement = connection.createStatement();
		String createTableSql = "create table if not exists message_history (" +
				"id integer primary key not null," +
				"create_datetime timestamp default current_timestamp," +
				"sender char(128)," +
				"message text)";
		statement.executeUpdate(createTableSql);
		statement.close();
	}


	@Override
	public void store(final Message message) {
		try {
			PreparedStatement statement = connection.prepareStatement("insert into message_history " +
					"(sender, message) " +
					"values (?, ?)");
			statement.setString(1, message.getSender());
			statement.setString(2, message.getText());
			statement.executeUpdate();
//			logMessageCount();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void logMessageCount() {
		ResultSet resultSet = null;
		try {
			resultSet = connection.createStatement().executeQuery("select count(*) as cnt from message_history");
			if (resultSet.next()) {
				logger.info(String.format("Message count %d", resultSet.getInt("cnt")));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
	}
}
