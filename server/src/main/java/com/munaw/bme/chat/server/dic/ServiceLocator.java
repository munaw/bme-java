package com.munaw.bme.chat.server.dic;

import com.munaw.bme.chat.api.Distributor;
import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.api.message.MessageImpl;
import com.munaw.bme.chat.server.Config;
import com.munaw.bme.chat.server.Server;
import com.munaw.bme.chat.server.client.ClientFactory;
import com.munaw.bme.chat.server.client.LogClient;
import com.munaw.bme.chat.server.client.MesssagePersisterClient;
import com.munaw.bme.chat.server.monitoring.MonitoringManager;
import com.munaw.bme.chat.server.services.DistributorImpl;
import com.munaw.bme.chat.server.services.SubscriptionServiceImpl;
import com.munaw.bme.chat.server.store.JdbcMessageDao;
import com.munaw.bme.chat.server.store.MessageDao;
import com.munaw.bme.chat.tools.SocketXmlChannelFactory;

import java.sql.SQLException;

/**
 * Dependency container implementáció imitáció Spring 4 mintára. Feladata az IoC, singleton szervízek példányosítása és
 * példányaik tárolása.
 * @author munaw
 */
public class ServiceLocator {
	final private Config config;
	private Server server;
	private SocketXmlChannelFactory socketXmlChannelFactory;
	private Distributor distributor;
	private SubscriptionService subscriptionService;
	private ClientFactory clientFactory;
	private MonitoringManager monitoringManager;
	private MesssagePersisterClient messsagePersisterClient;
	private LogClient logClient;

	public ServiceLocator(final Config config) {
		this.config = config;
	}

	public synchronized Server getServer() {
		if (server == null) {
			server = new Server(getServerPort(), getServerMaxClient(), getDistributor(), getClientFactory(), getChannelFactory(), getSubscriptionService());
		}
		return server;
	}

	public int getServerMaxClient() {
		return config.getMaxClient();
	}

	public int getServerPort() {
		return config.getPort();
	}

	public synchronized SocketXmlChannelFactory getChannelFactory() {
		if (socketXmlChannelFactory == null) {
			socketXmlChannelFactory = new SocketXmlChannelFactory(getMessageClass());
		}
		return socketXmlChannelFactory;
	}

	public Class<MessageImpl> getMessageClass() {
		return MessageImpl.class;
	}

	public synchronized Distributor getDistributor() {
		if (distributor == null) {
			distributor = new DistributorImpl(getDistributorMaxQueueSize(), getSubscriptionService());
		}
		return distributor;
	}

	public synchronized SubscriptionService getSubscriptionService() {
		if (subscriptionService == null) {
			subscriptionService = new SubscriptionServiceImpl();
		}
		return subscriptionService;
	}

	public synchronized ClientFactory getClientFactory() {
		if (clientFactory == null) {
			clientFactory = new ClientFactory();
		}
		return clientFactory;
	}

	public int getDistributorMaxQueueSize() {
		return 1000; // most beegetve
	}

	public synchronized MonitoringManager getMonitoringManager() {
		if (monitoringManager == null) {
			monitoringManager = new MonitoringManager(getSubscriptionService(), getLogClient(), getMesssagePersisterClient());
		}
		return monitoringManager;
	}

	public synchronized LogClient getLogClient() {
		if (logClient == null) {
			logClient = new LogClient();
		}
		return logClient;
	}

	public synchronized MesssagePersisterClient getMesssagePersisterClient() {
		if (messsagePersisterClient == null) {
			messsagePersisterClient = new MesssagePersisterClient(getMessageDao());
		}
		return messsagePersisterClient;
	}

	public synchronized MessageDao getMessageDao() {
		JdbcMessageDao jdbcMessageDao = null;
		try {
			jdbcMessageDao = new JdbcMessageDao(getDbDrive(), getDbConnection());
		} catch (ClassNotFoundException | SQLException e) {
			throw new RuntimeException("Unable to instantiate MessageDao bean.", e);
		}
		return jdbcMessageDao;
	}

	public String getDbDrive() {
		return config.getDbDrive();
	}

	public String getDbConnection() {
		return config.getDbConnection();
	}
}
