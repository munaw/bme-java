package com.munaw.bme.chat.server.client;

import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.api.consumer.Consumer;
import com.munaw.bme.chat.api.consumer.ConsumerId;
import com.munaw.bme.chat.api.message.DefaultPartyId;
import com.munaw.bme.chat.api.message.Message;
import org.apache.log4j.Logger;

/**
 * Loggoló kliens implementáció, mely a fogadott üzeneteket a logger felé továbbítja
 * @author munaw
 */
public class LogClient implements Consumer {

	private final ConsumerId consumerId = new DefaultPartyId(Client.IdGenerator.generateId());

	private final static Logger logger = Logger.getLogger(LogClient.class);

	@Override
	public ConsumerId getConsumerId() {
		return consumerId;
	}

	@Override
	public void onMessage(final Message message, final SubscriptionService subscriptionService) {
		logger.info("Message: " + message);
	}


}
