package com.munaw.bme.chat.server.client;

import com.munaw.bme.chat.api.Channel;
import com.munaw.bme.chat.api.Distributor;

/**
 * Kliens példányosító osztály
 * @author munaw
 */
public class ClientFactory {
	public Client createClient(Distributor distributor, Channel channel) {
		return new Client(distributor, channel);
	}
}
