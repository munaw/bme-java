package com.munaw.bme.chat.server.client;


import com.munaw.bme.chat.api.Channel;
import com.munaw.bme.chat.api.Distributor;
import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.api.consumer.Consumer;
import com.munaw.bme.chat.api.consumer.ConsumerId;
import com.munaw.bme.chat.api.message.DefaultPartyId;
import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.api.producer.Producer;
import com.munaw.bme.chat.api.producer.ProducerId;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Egy kapcsolódott klienst reprezentáló objektum egy Client. A kliens két műveletet végez:
 * - Consumerként felkerül a SubscriptionService listájára és fogad üzeneteket
 * - Producerként használja a Distributort és amennyiben üzenet érkezik a kapcsolódott klienstől, továbbítja a distribútor felé
 * Kiterjeszti a Runnable-t, hiszen a klienstől érkező üzenetek olvasását külön szálon végzi kliensenként.
 *
 * @author munaw
 */
public class Client implements Consumer, Producer, Runnable {
	final private Distributor distributor;
	final private Channel channel;
	final private Long id;
	final private DefaultPartyId partyId;

	final private static Logger logger = Logger.getLogger(Client.class);


	public Client(final Distributor distributor, Channel channel) {
		this.distributor = distributor;
		this.channel = channel;
		id = IdGenerator.generateId();
		partyId = new DefaultPartyId(id);
	}

	@Override
	public ProducerId getProducerId() {
		return partyId;
	}

	@Override
	public ConsumerId getConsumerId() {
		return partyId;
	}

	@Override
	public void onMessage(final Message message, SubscriptionService subscriptionService) {
		try {
			channel.send(message);
		} catch (IOException e) {
			subscriptionService.unregister(this);
		}
	}

	@Override
	public void run() {
		while (true) {
			try {
				Message message = null;
				try {
					message = channel.receive();
					distributor.put(this, message);
				} catch (RuntimeException e) {
					logger.error("Hiba fogadaskor", e);
				}
			} catch (InterruptedException | IOException e) {
				logger.error("Kliens lebontott vagy a szerver leallitasra kerul.");
				return;
			}
		}
	}

	static class IdGenerator {
		private static AtomicLong idSeq = new AtomicLong();

		static public long generateId() {
			return idSeq.incrementAndGet();
		}
	}
}
