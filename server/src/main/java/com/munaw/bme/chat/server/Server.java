package com.munaw.bme.chat.server;

import com.munaw.bme.chat.api.Distributor;
import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.server.client.Client;
import com.munaw.bme.chat.server.client.ClientFactory;
import com.munaw.bme.chat.tools.SocketXmlChannelFactory;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Szerver osztály.
 * A szervernek két fő feladata van:
 * - socket kapcsolatok fogadása
 * - a kapcsolódásokból kliens-reprezentánsok létrehozása
 * - kliens workerek indítása az új kapcsolathoz
 *
 * @author munaw
 */
public class Server {
	final static private Logger logger = Logger.getLogger(Server.class);
	final private int port;
	final private int maxClient;
	final private Distributor distributor;
	final private ClientFactory clientFactory;
	final private Executor executor;
	final private SocketXmlChannelFactory channelFactory;
	final private SubscriptionService subscriptionService;

	public Server(final int port, final int maxClient, final Distributor distributor, ClientFactory clientFactory, SocketXmlChannelFactory channelFactory, final SubscriptionService subscriptionService) {
		this.port = port;
		this.maxClient = maxClient;
		this.distributor = distributor;
		this.clientFactory = clientFactory;
		this.channelFactory = channelFactory;
		this.subscriptionService = subscriptionService;
		executor = Executors.newFixedThreadPool(maxClient);
	}

	/**
	 * A szerver a start metódusával indítható, mely nem tér  vissza.
	 * @throws IOException
	 * @throws JAXBException
	 */
	public void start() throws IOException, JAXBException {
		executor.execute(distributor);

		ServerSocket serverSocket = new ServerSocket(port);
		Socket clientSocket;
		logger.info(String.format("Chat server fut a %d-os porton", port));
		for (; ; ) {
			clientSocket = serverSocket.accept();
			logger.info("Uj kliens csatlakozott");
			Client client = clientFactory.createClient(distributor, channelFactory.createChannel(clientSocket));
			subscriptionService.register(client);
			executor.execute(client);
		}
	}

}
