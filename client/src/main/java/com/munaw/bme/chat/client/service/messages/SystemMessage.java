package com.munaw.bme.chat.client.service.messages;

import com.munaw.bme.chat.api.message.Message;

/**
 * @author munaw
 */
public interface SystemMessage extends Message {
	public enum Type {
		CONNECTED("Kapcsolodva"),
		RECONNECTING("Újrakapcsolódás folyamatban..."),
		RECONNECTED("Kapcsolat helyreállt"),
		DISCONNECTED("Kapcsolat megszakadt");
		private String name;

		Type(final String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	;

	Type getType();
}
