package com.munaw.bme.chat.client;

import com.munaw.bme.chat.api.Channel;
import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.api.message.MessageImpl;
import com.munaw.bme.chat.tools.SocketXmlChannelFactory;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

/**
 * Fejlesztést támogató tesztalkalmazás
 * @author munaw
 */
public class TestClientApplication {
	static private final Logger logger = Logger.getLogger(TestClientApplication.class);

	public static void main(String[] args) {
		while (true) {
			try {
				new TestClientApplication().start();
			} catch (Exception e) {
				logger.warn("Exception. reconnect in 5s", e);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					return;
				}
			}
		}
	}

	private void start() throws IOException, JAXBException, InterruptedException {
		try {
			Socket socket = new Socket("localhost", 8383);
			while (!socket.isConnected()) ;
			SocketXmlChannelFactory socketXmlChannelFactory = new SocketXmlChannelFactory(MessageImpl.class);
			Channel channel = socketXmlChannelFactory.createChannel(socket);
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						while (true) {
							channel.send(
									new MessageImpl.Builder()
											.withSender(String.valueOf((Math.random() * 10000)))
											.build()
							);
							logger.info("Uzenet elkuldve");
							Thread.sleep(1000);
						}
					} catch (IOException e) {
						return;
					} catch (InterruptedException e) {
						return;
					}
				}
			}).start();


			while (true) {
				Message message = channel.receive();
				logger.info("message received: " + message.toString());
			}

		} catch (ConnectException e) {
			throw e;
		}
	}
}
