package com.munaw.bme.chat.client.service.messages;

/**
 * @author munaw
 */
public class ReconnectingMessage extends AbstractSystemMessage {
	public ReconnectingMessage() {
		super(Type.RECONNECTING);
	}
}
