package com.munaw.bme.chat.client;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * Konfigurációt felolvasó és azt tároló osztály.
 * @author munaw
 */
public class Config {
	final private Properties properties;

	final private int port;
	final private String host;


	public Config(final String filename, final Map<String, String> argsMap) throws IOException {
		properties = new Properties();
		properties.load(getClass().getClassLoader().getResourceAsStream(filename));
		port = argsMap.containsKey("port")
				? Integer.parseInt(argsMap.get("port"))
				: Integer.parseInt(properties.getProperty("port"));
		host = argsMap.containsKey("host")
				? argsMap.get("host")
				: properties.getProperty("host");


	}

	public int getPort() {
		return port;
	}

	public String getHost() {
		return host;
	}
}
