package com.munaw.bme.chat.client.exception;

/**
 * Belső kivétel, melyet a kliens-szervíz dob, ha megszakad a kapcsolat, vagy nincs küldés idején
 * @author munaw
 */
public class NotConnectedException extends Exception {
	public NotConnectedException() {
	}

	public NotConnectedException(final String message, final Exception e) {
		super(message, e);
	}
}
