package com.munaw.bme.chat.client.controller;

import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.api.message.MessageImpl;
import com.munaw.bme.chat.client.exception.NotConnectedException;
import com.munaw.bme.chat.client.service.ChatClient;
import com.munaw.bme.chat.client.view.ChatView;
import com.munaw.bme.chat.client.view.InitDialog;
import org.apache.log4j.Logger;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Chat ablakok vezérlője.
 * Indításkor megjeleníti a névválasztó ablakot és a beírt névvel elindítja a chatelést.
 * Elvégzi cliens szervíz és a UI közt a kommunikációt. Lebontott kapcsolat esetén újra nyitja a kapcsolatot
 * @author munaw
 */
public class ChatController {
	static private final Logger logger = Logger.getLogger(ChatController.class);
	/**
	 * Chat ablak JFrame objektum
	 */
	private ChatView chatView;
	/**
	 * Névbekérő JDialog objektum
	 */
	private InitDialog initDialog;
	/**
	 * A felhasználó által megadott nevet tárolja
	 */
	private String senderName;
	/**
	 * A ChatClient szervíz példánya
	 */
	private final ChatClient chatClient;

	/**
	 * A használathoz át kell adni egy ChatClient szervíz példányt
	 * @param chatClient
	 */
	public ChatController(ChatClient chatClient) {
		this.chatClient = chatClient;
	}

	/**
	 * A metódus meghívásával indítható a kontroller és az első képernyő
	 */
	public void start() {
		initNameDialog();
	}

	/**
	 * Kirajzolja a névbekérő ablakot
	 */
	private void initNameDialog() {
		initDialog = new InitDialog(
				(name) ->
				{
					senderName = name;
					initChatView();
				}
				, this::exit
		)
		;
	}

	/**
	 * Meghívódik, ha a névbekérő ablakban mégse gombot nyomnak
	 */
	private void exit() {

	}

	/**
	 * Kirajzolja a chat ablakot
	 */
	private void initChatView() {
		chatView = new ChatView();
		chatView.setSenderName(senderName);
		chatView.setVisible(true);
		chatView.onSend(this::onSend);

		connectOrRetry();
	}

	/**
	 * Újrapróbálós kapcsolódást végez
	 */
	private void connectOrRetry() {
		try {
			chatClient.start(this::onReceive);
		} catch (NotConnectedException e) {
			chatView.appendSysMessage("Csatlakozás sikertelen (szerver fut a megfelelő címen és porton?). 5 s múlva újra próbálom.");
			logger.error("Connection failed", e);
			new Timer().schedule(new TimerTask() {
				@Override
				public void run() {
					connectOrRetry();
				}
			}, 5000L);
		}
	}

	/**
	 * Ez a metódus hívódik üzenet érkezése esetén
	 * @param message Az érkező üzenet
	 */
	private void onReceive(Message message) {
		chatView.append(message);
	}

	/**
	 * Ez a metódus hívódik, amikor a UI-on új üzenetet kíván küldeni a felhasználó. A kontroller
	 * az üzenetet továbbítja a kliens-szervíz felé. Ha nincs kapcsolat, letiltja a UI-t.
	 * @param messageText Elküldendő üzenet szövege.
	 */
	private void onSend(String messageText) {
		Message message = new MessageImpl.Builder().withSender(senderName).withText(messageText).build();
		try {
			chatClient.send(message);
			chatView.append(message);
			chatView.clearInput();
		} catch (NotConnectedException e) {
			chatView.disableScreen();
		}
	}

}
