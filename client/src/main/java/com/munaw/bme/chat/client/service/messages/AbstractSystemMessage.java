package com.munaw.bme.chat.client.service.messages;

/**
 * @author munaw
 */
abstract public class AbstractSystemMessage implements SystemMessage {
	final private Type type;

	protected AbstractSystemMessage(final Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	@Override
	public String getSender() {
		return null;
	}

	@Override
	public String getText() {
		return type.toString();
	}
}
