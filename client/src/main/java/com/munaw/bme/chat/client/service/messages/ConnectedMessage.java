package com.munaw.bme.chat.client.service.messages;

/**
 * @author munaw
 */
public class ConnectedMessage extends AbstractSystemMessage {
	public ConnectedMessage() {
		super(Type.CONNECTED);
	}
}
