package com.munaw.bme.chat.client;

import com.munaw.bme.chat.api.message.MessageImpl;
import com.munaw.bme.chat.client.controller.ChatController;
import com.munaw.bme.chat.client.service.ChatClient;
import com.munaw.bme.chat.tools.SocketXmlChannelFactory;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * @author munaw
 */
public class ClientApplication {
	static private final Logger logger = Logger.getLogger(ClientApplication.class);

	public static void main(String[] args) {
		Cli cli = new Cli();
		Map<String, String> argsMap = null;
		try {
			argsMap = cli.parse(args);
		} catch (ParseException e) {
			e.printStackTrace();
			return;
		}
		if (argsMap.containsKey("help")) {
			cli.help();
			return;
		}
		Config config;
		try {
			config = new Config("application.properties", argsMap);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		ChatClient chatClient = new ChatClient(config.getHost(), config.getPort(), new SocketXmlChannelFactory(MessageImpl.class));
		ChatController chatController = new ChatController(chatClient);
		chatController.start();
	}

	/**
	 * Konzol parancsokat kezelő osztály
	 */
	static private class Cli {
		private final Options options;

		private Cli() {
			options = new Options();
			options.addOption("h", "help", false, "Help");
			options.addOption("p", "port", true, "A port, amin a szerver figyel");
			options.addOption("s", "server-address", true, "Szerver cime");
		}

		public Map<String, String> parse(String[] args) throws ParseException {
			BasicParser parser = new BasicParser();
			CommandLine cmd = parser.parse(options, args);
			Map<String, String> params = new HashMap<>();
			if (cmd.hasOption("p")) {
				params.put("port", cmd.getOptionValue("p"));
			}
			if (cmd.hasOption("s")) {
				params.put("host", cmd.getOptionValue("s"));
			}
			if (cmd.hasOption("h")) {
				params.put("help", cmd.getOptionValue("h"));
			}
			return params;
		}

		private void help() {
			HelpFormatter formater = new HelpFormatter();
			formater.printHelp("client", options);
			System.exit(0);
		}

	}
}
