package com.munaw.bme.chat.client.service;

import com.munaw.bme.chat.api.Channel;
import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.client.TestClientApplication;
import com.munaw.bme.chat.client.exception.NotConnectedException;
import com.munaw.bme.chat.client.service.messages.ConnectedMessage;
import com.munaw.bme.chat.client.service.messages.DisconnectedMessage;
import com.munaw.bme.chat.client.service.messages.ReconnectedMessage;
import com.munaw.bme.chat.client.service.messages.ReconnectingMessage;
import com.munaw.bme.chat.tools.SocketXmlChannelFactory;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

/**
 * Kapcsolódást kezeli a kliensek számára. Kezel két külön szálat a kapcsolódás után az üzenetek fogadására,
 * továbbá a kiküldendő üzenetek blokkolásmentes küldésére.
 *
 * @author munaw
 */
public class ChatClient {
	static private final Logger logger = Logger.getLogger(TestClientApplication.class);
	private final String host;
	private final int port;
	private final SocketXmlChannelFactory socketXmlChannelFactory;
	private Socket socket;
	private Channel channel;


	private Thread listenerThread;
	private Thread publisherThread;

	private Consumer<Message> messageConsumer;

	private final BlockingQueue<Message> messageQueue;

	public ChatClient(final String host, final int port, SocketXmlChannelFactory socketXmlChannelFactory) {
		this.host = host;
		this.port = port;
		this.socketXmlChannelFactory = socketXmlChannelFactory;
		this.messageQueue = new LinkedBlockingQueue<>(1000);

		this.listenerThread = new Thread(this::listen); //java8
		this.publisherThread = new Thread(new Publisher()); //java7
		this.publisherThread.setDaemon(true);
	}

	/**
	 * Ezzel a setterrel allithato be üzeneteket fogadó Consumer.
	 * @param messageConsumer
	 */
	public synchronized void setMessageConsumer(final Consumer<Message> messageConsumer) {
		this.messageConsumer = messageConsumer;
	}

	public synchronized void connect() throws IOException {
		socket = new Socket(host, port);
		try {
			channel = socketXmlChannelFactory.createChannel(socket);
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
		messageQueue.add(new ConnectedMessage());
		logger.info("Kapcsolodva a szerverhez");
	}

	/**
	 * Elindítja a klienst. Létrejön a kapcsolat és a figyelő, publikáló threadek is elindulnak.
	 * @param messageConsumer
	 * @throws NotConnectedException
	 */
	public void start(Consumer<Message> messageConsumer) throws NotConnectedException {
		setMessageConsumer(messageConsumer);
		if (!isConnected()) {
			try {
				connect();
			} catch (IOException e) {
				throw new NotConnectedException(e.getMessage(), e);
			}
		}
		listenerThread.start();
		publisherThread.start();
	}


	public synchronized boolean isConnected() {
		return socket != null && socket.isConnected() && !socket.isClosed();
	}

	public synchronized void send(Message message) throws NotConnectedException {
		if (!isConnected()) {
			throw new NotConnectedException();
		}
		try {
			channel.send(message);
		} catch (IOException e) {
			logger.error("Exception while sending message", e);
			throw new NotConnectedException(e.getMessage(), e);
		}
	}

	/**
	 * Ez a metodus fut a listenerThread-ben és figyeli
	 * a szerverről érkező üzeneteket, majd továbbírja a messageQueue-ba.
	 * Ha lebont a kapcsolat, megpróbál újrakapcsolódni.
	 * Ez a metódus java8 stílusban használt.
	 */
	private void listen() {
		while (true) {
			try {
				try {
					Message message = channel.receive();
					messageQueue.add(message);
				} catch (RuntimeException | IOException e) {
					logger.error("Kapcsolati hiba", e);

					if (!isConnected()) {
						messageQueue.add(new DisconnectedMessage());
						int tryCount = 10;
						while (tryCount-- > 0) {
							messageQueue.add(new ReconnectingMessage());
							logger.warn("ujrakapcsolodas");
							try {
								connect();
								messageQueue.add(new ReconnectedMessage());
								break;
							} catch (IOException e1) {
								logger.error("ujrakapcsolodas nem sikerult", e1);
								Thread.sleep(2000);
							}
						}
					}
				}
			} catch (InterruptedException e) {
				logger.warn("Szal megszakitva", e);
			}

		}
	}

	/**
	 * A szerverről érkezett üzeneteket külön threadben publikáló metódus/runnable.
	 */
	private class Publisher implements Runnable {
		public void run() {
			try {
				while (true) {
					Message message = messageQueue.take();
					if (messageConsumer != null) {
						messageConsumer.accept(message);
					}
				}
			} catch (InterruptedException e) {
			}
		}
	}
}
