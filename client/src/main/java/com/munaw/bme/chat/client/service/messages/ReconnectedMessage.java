package com.munaw.bme.chat.client.service.messages;

/**
 * @author munaw
 */
public class ReconnectedMessage extends AbstractSystemMessage {
	public ReconnectedMessage() {
		super(Type.RECONNECTED);
	}
}
