package com.munaw.bme.chat.client.service.messages;

/**
 * @author munaw
 */
public class DisconnectedMessage extends AbstractSystemMessage {
	public DisconnectedMessage() {
		super(Type.DISCONNECTED);
	}
}
