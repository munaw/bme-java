package com.munaw.bme.chat.api;

import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.api.message.MessageImpl;
import com.munaw.bme.chat.tools.SocketXmlChannel;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SocketXmlChannelTest {
	@Mock
	private Socket socket;
	@Mock
	private OutputStream outputStream;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	@Ignore("fixme")
	public void testSend() throws Exception {
		SocketXmlChannel sut = getSut();
		TestCase.assertNotNull(outputStream);
		when(socket.getOutputStream()).thenReturn(outputStream);

		Message testMessage = getTestMessage();
		sut.send(testMessage);
//		verify(outputStream.);
	}

	private Message getTestMessage() {
		return new MessageImpl.Builder().withId(123L).withSender("sender").withText("Text").build();
	}

	private SocketXmlChannel getSut() throws IOException, JAXBException {
		return new SocketXmlChannel(socket, MessageImpl.class);
	}

	@Test
	public void testReceive() throws Exception {

	}
}