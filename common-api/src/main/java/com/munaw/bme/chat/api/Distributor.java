package com.munaw.bme.chat.api;

import com.munaw.bme.chat.api.message.Message;
import com.munaw.bme.chat.api.producer.Producer;

/**
 * A disztribútor interface-t implementáló osztály továbbítandó üzeneteket fogadja
 * a feladó Producer megadásával.
 *
 * @author munaw
 */
public interface Distributor extends Runnable {
	/**
	 * A put metódusnak átadott üzeneteket a Distributer továbbítja.
	 * @param sender Feladó
	 * @param message Üzenet
	 */
	void put(Producer sender, Message message);
}
