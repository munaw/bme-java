package com.munaw.bme.chat.tools;

import com.munaw.bme.chat.api.Channel;
import com.munaw.bme.chat.api.message.Message;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * TCP socketen, XML formátumban kommunikáló Channel implementáció.
 * @author munaw
 */
public class SocketXmlChannel implements Channel {
	private final Socket socket;
	private final InputStream inputStream;
	private final OutputStream outputStream;
	private final Unmarshaller unmarshaller;
	private final Marshaller marshaller;
	private final Class<? extends Message> messageClass;
	private final static Logger logger = Logger.getLogger(SocketXmlChannel.class);

	public SocketXmlChannel(final Socket socket, Class<? extends Message> messageClass) throws IOException, JAXBException {
		this.socket = socket;
		inputStream = socket.getInputStream();
		this.messageClass = messageClass;
		JAXBContext messageContext = JAXBContext.newInstance(messageClass);
		marshaller = messageContext.createMarshaller();
		unmarshaller = messageContext.createUnmarshaller();
		outputStream = new BufferedOutputStream(socket.getOutputStream());
	}


	@Override
	public synchronized void send(final Message message) throws IOException {
		try {
			marshaller.marshal(message, outputStream);
			outputStream.flush();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Message receive() throws InterruptedException, IOException {
		try {
			inputStream.available();
			XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
			XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(inputStream);
			Message message = (Message) unmarshaller.unmarshal(xmlEventReader);
			logger.trace("Message received: " + message.toString());
			return message;
		} catch (JAXBException | XMLStreamException e) {
			throw new RuntimeException(e);
		}
	}
}
