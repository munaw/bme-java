package com.munaw.bme.chat.api;

import com.munaw.bme.chat.api.message.Message;

import java.io.IOException;

/**
 * Kommunikációs csatorna interfész. Absztrakt interfész szinkron, blokkoló kommunikációhoz.
 * @author munaw
 */
public interface Channel {
	/**
	 * Üzenetküldés metódus
	 * @param message Küldendő üzenet
	 * @throws IOException Kapcsolati hiba esetén váltódik ki.
	 */
	void send(Message message) throws IOException;

	/**
	 * Blokkoló fogadást tesz lehetővé. A metódus akkor tér vissza, ha fogadott a csatorna egy üzenetet.
	 * @return Fogadott üzenet
	 * @throws InterruptedException Szálmegszakítás esetén
	 * @throws IOException Kapcsolati hiba esetén.
	 */
	Message receive() throws InterruptedException, IOException;
}
