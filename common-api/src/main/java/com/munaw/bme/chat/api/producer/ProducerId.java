package com.munaw.bme.chat.api.producer;

import com.munaw.bme.chat.api.message.PartyId;

/**
 * Üzenetküldő azonosító interfész
 * @author munaw
 */
public interface ProducerId extends PartyId {
	String getName();
}
