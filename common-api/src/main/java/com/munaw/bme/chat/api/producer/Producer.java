package com.munaw.bme.chat.api.producer;

/**
 * Üzenet küldő interfésze.
 * @author munaw
 */
public interface Producer {
	/**
	 * Megadja az üzenetküldő egyedi azonosítóját
	 * @return
	 */
	ProducerId getProducerId();
}
