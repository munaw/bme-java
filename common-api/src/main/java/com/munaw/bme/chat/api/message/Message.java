package com.munaw.bme.chat.api.message;

/**
 * Üzenet reprezentációs objektum interfésze
 * @author munaw
 */
public interface Message {
	String getSender();

	String getText();
}
