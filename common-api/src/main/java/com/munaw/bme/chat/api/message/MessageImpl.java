package com.munaw.bme.chat.api.message;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * Üzenet reprezentációs objektum
 * XML átvitelhez szükséges JAXB annotiációkkal ellátott.
 *
 * Nem módosítható típus, csak Builder vagyis építő segítségével hozható létre.
 *
 * @immutable
 * @author munaw
 */
@XmlRootElement(name = "message")
public class MessageImpl implements Message {
	@XmlTransient
	final private Long id;
	@XmlElement
	final private String sender;
	@XmlElement
	final private String text;


	private MessageImpl() {
		id = null;
		sender = null;
		text = null;
	}

	private MessageImpl(final Builder builder) {
		id = builder.id;
		sender = builder.sender;
		text = builder.text;
	}


	public Long getId() {
		return id;
	}

	@Override
	public String getSender() {
		return sender;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return "MessageImpl{" +
				"id=" + id +
				", sender='" + sender + '\'' +
				", text='" + text + '\'' +
				'}';
	}

	/**
	 * Immutable patterhez tartozó Builder
	 */
	public static class Builder {
		public Long id;
		private String sender;
		private String text;

		public Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withSender(String sender) {
			this.sender = sender;
			return this;
		}

		public Builder withText(String text) {
			this.text = text;
			return this;
		}

		public Message build() {
			return new MessageImpl(this);
		}
	}
}
