package com.munaw.bme.chat.api.message;

import com.munaw.bme.chat.api.consumer.ConsumerId;
import com.munaw.bme.chat.api.producer.ProducerId;

/**
 * Fogyasztó és Küldő azonosító implementáció Long alapon.
 * @author munaw
 */
public class DefaultPartyId implements ConsumerId, ProducerId {
	final private Long id;

	public DefaultPartyId(final Long id) {
		this.id = id;
	}


	@Override
	public String getName() {
		return String.valueOf(id);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		final DefaultPartyId that = (DefaultPartyId) o;

		if (!id.equals(that.id)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
