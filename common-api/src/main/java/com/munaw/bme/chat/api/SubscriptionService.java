package com.munaw.bme.chat.api;

import com.munaw.bme.chat.api.consumer.Consumer;

import java.util.List;

/**
 * Az implementáció feladata tárolni és ThreadSafe módon kezelni az üzeneteket fogadó klienseket
 *
 * @author munaw
 */
public interface SubscriptionService {
	/**
	 * Fogyasztó feljelentkeztetése.
	 * @param consumer
	 */
	void register(Consumer consumer);

	/**
	 * Fogyasztó eltávolítása
	 * @param consumer
	 */
	void unregister(Consumer consumer);

	/**
	 * Fogyasztó-lista másolatának legkérése
	 * @return Fogyasztólista
	 */
	List<Consumer> getConsumers();
}
