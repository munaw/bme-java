package com.munaw.bme.chat.tools;

import com.munaw.bme.chat.api.Channel;
import com.munaw.bme.chat.api.message.Message;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.Socket;

/**
 * SocketXmlChannel példányosító osztály
 * @author munaw
 */
public class SocketXmlChannelFactory {
	private final Class<? extends Message> messageClass;

	public SocketXmlChannelFactory(final Class<? extends Message> messageClass) {
		this.messageClass = messageClass;
	}


	public Channel createChannel(Socket socket) throws IOException, JAXBException {
		return new SocketXmlChannel(socket, messageClass);
	}
}
