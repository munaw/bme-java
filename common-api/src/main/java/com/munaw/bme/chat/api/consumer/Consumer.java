package com.munaw.bme.chat.api.consumer;

import com.munaw.bme.chat.api.SubscriptionService;
import com.munaw.bme.chat.api.message.Message;

/**
 * Üzenetet fogadni képes típusok interfésze.
 * @author munaw
 */
public interface Consumer {
	ConsumerId getConsumerId();

	/**
	 * Ez a metódus hívódik, amikor egy üzenet kerül átadásra.
	 * @param message
	 * @param subscriptionService
	 */
	void onMessage(Message message, SubscriptionService subscriptionService);
}
