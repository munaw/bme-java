package com.munaw.bme.chat.api.consumer;

import com.munaw.bme.chat.api.message.PartyId;

/**
 * Fogyasztó azonosító típus interfésze
 * @author munaw
 */
public interface ConsumerId extends PartyId {
}
