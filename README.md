# Chat klies es szerver (BME java hazi) #

## Fordítás ##
Az alkalmazás lefordításához JDK 8-ra és Maven3-ra van szükség.

```
#!bash

mvn clean package
```

Build után a következő futtatható komponensek készülnek:
* Szerver: server/target/server-1.0-jar-with-dependencies.jar
* Kliens: client/target/client-1.0-jar-with-dependencies.jar
## Hasznalat ##
### Szerver
A kliens indítása előtt egy szervert szükséges elindítani. Ezt az alábbi paranccsal lehet

```
#!bash

java -jar server/target/server-1.0-jar-with-dependencies.jar
```
Az elérhető paramétereket a következőképp lehet lekérni.

```
#!bash

java -jar server/target/server-1.0-jar-wh-dependencies.jar -h
usage: server
 -c,--db-connection <arg>   DB connection string
 -d,--db-driver <arg>       DB driver
 -h,--help                  Help
 -m,--max-client <arg>      Max client connection
 -p,--port <arg>            A port, amin a szerver figyel
```
Alapértelmezés szerint a szerver a 8383-as porton indul. Ez megváltoztatható a -p paraméterrel.

```
#!bash

java -jar server/target/server-1.0-jar-wh-dependencies.jar -p 9595
2015-04-21 00:25:24 INFO  Server:49 - Chat server fut a 9595-os porton

```


### Kliens
A klien alapértelmezetten a localhost:8383-as címen lévő szerverhez kapcsolódik. Indítani a következőképp lehet:

```
#!bash

java -jar client/target/client-1.0-jar-with-dependencies.jar
```
Az elérhető paramétereket a következőképp lehet lekérni.

```
#!bash

java -jar client/target/client-1.0-jar-with-dependencies.jar -h
usage: client
 -h,--help                   Help
 -p,--port <arg>             A port, amin a szerver figyel
 -s,--server-address <arg>   Szerver cime
```

Az után a grafikus felület megjelenik. Először az üzenetküldéshez használandó nevet kell megadni, majd az alkalmazás csatlakozik a szerverhez.

## Technikai leírás ##

Az alkalmazás forrása három maven modulból áll. A client a GUI-t, a server a chat elosztó szervert, a common-api pedig az első kettő által közösen használt interfészeket és kódokat tartalmazza.
### Szerver ###
A szerver alkalmazás minden komponense IoC (Inversion of Control) elv szerint került specifikálva és implementálva. A dependency injectiont a ServiceLocator osztály végzi. Az alkalmazás a ServerApplication::main osztódussal indul, melyben a Config, a ServiceLocator és a Server osztályok kerülnek példányosításra.

A főbb osztályok leírása a fájlok tetején olvasható.

### Kliens ###
A kliens vezérlését ClientApplication után a ChatController veszi át. A vezérlő először bekéri a nevet az InitDialog segítségével, majd annak megadása után inicializálja a kapcsolódást a ChatClient segítéségével. A chat-ablakot a ChatView reprezentálja, mely egy IntelliJ GUI Designer form fájl generátumának és a hozzáadott megjelenítési logikának a hibride.

A sikertelen kapcsolódás újbóli próbálkozását a ChatController végzi. Amennyiben sikeres kapcsolódás után a kapcsolat megszakad, a ChatClient ezt érzékeli fogadás és küldés oldalon is, majd a kapcsolatot folyamatosan próbálja újra felvenni a szerverrel. A kapcsolódás eseményeiről tovább tájékoztatja a kapcsolat figyelőjét rendszerüzeneteken keresztül. A ChatClient emellett külön szálakon kezeli az üzenetek fogadását és a továbbítandó üzenetek elküldését. Ennek köszönhetően az üzenetek fogadása és küldése is blokkolás-mentes működésű.

## Követelmények ##
### Kötelezőek ###
* Szabályos kivételkezelés (a keletkezo kivételek - az ésszerűség határain belül - le vannak kezelve)
* Csomagok használata: maven
* Collections Framework:
* * BlockingQueue a Distributorban
* * SubscriptionService collectionben tárol, másolatot ad ki.
* Stream I/O
* * A JAXB parser inputStreamet olvas
* * a Config reader inputStreamet olvas
* Logging
* * Log4j
* Swing (javax.swing)
* * Kliens felület

### Választhatók ###
* Hálózatkezelés
* * Socket kapcsolat client-server között
* JDBC (Java Database Connectivity)
* * MessageDao implementációja SQLite JDBC
* Preferences / Properties
* * Config osztály
* XML
* * Kommunikáció JAXB alapú